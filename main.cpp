#include <iostream>
#include <windows.h>
#include <cstdlib>
#include <conio.h>
#include <string>
#include <ctime>
#include "header.h"
using namespace std;
void gotoxy( int column, int line );
struct Point{
    int x,y;
};
class CONRAN{
public:
    struct Point A[100];
    int DoDai;
    CONRAN(){
        DoDai = 3;
        A[0].x = 10; A[0].y = 10;
        A[1].x = 11; A[1].y = 10;
        A[2].x = 12; A[2].y = 10;
    }
    
    void Ve(Point T){
        for (int i = 0; i < DoDai; i++)
		{
			TextColor(ColorCode_DarkPink);
            gotoxy(A[i].x,A[i].y);
            cout<<"0";
        }
        gotoxy(T.x,T.y);
        cout << " ";
    }
    
    Point DiChuyen(int Huong)
    {
		Point ToaDoCuoiCu = A[DoDai - 1];
        for (int i = DoDai-1; i>0;i--)
            A[i] = A[i-1];
        if (Huong==0) A[0].x = A[0].x + 1;
        if (Huong==1) A[0].y = A[0].y + 1;
        if (Huong==2) A[0].x = A[0].x - 1;
        if (Huong==3) A[0].y = A[0].y - 1;
		return ToaDoCuoiCu;
    }
    
    void vetuong()
	{
		
		for (int i = Trai; i <= Phai; i++)
		{
			TextColor(ColorCode_DarkBlue);
			gotoxy(i, Tren);
			cout << "X";
		}
		for (int i = Tren; i <= Duoi; i++)
		{
			TextColor(ColorCode_DarkBlue);
			gotoxy(Trai, i);
			cout << "X";
		}
		for (int i = Trai; i <= Phai; i++)
		{
			TextColor(ColorCode_DarkBlue);
			gotoxy(i, Duoi);
			cout << "X";
		}
		for (int i = Tren; i <= Duoi; i++)
		{
			TextColor(ColorCode_DarkBlue);
			gotoxy(Phai, i);
			cout << "X";
		}
	}
	
	bool kiemtraThua()
	{
		if (A[0].y == Tren)
			return true;
		if (A[0].y == Duoi)
			return true;
		if (A[0].x == Trai)
			return true;
		if (A[0].x == Phai)
			return true;
       /* for (int i=0; i<DoDai; i++)
            if(A[0].x == A[i].x && A[0].y == A[i].y) return true; */
		return false;
	}
	void xuliThua()
	{
		if (kiemtraThua())
		{
			Sleep(500);
			system("cls");
			cout << "\t\t\t\t\t\tGAME OVER!!!!!" << endl;
		}
	}

	Point Moi()
	{
	    bool check=1;
	    int x,y;
	    do
        {
		srand(time(NULL));
		x = Trai + 1 + rand() % (Phai - Trai - 1);
		y = Tren + 1 + rand() % (Duoi - Tren - 1);
		for (int i = 0; i < DoDai; i++)
            if(A[i].x == x && A[i].y == y)
            {
                check=0;
            }
        } while (check==0);
		gotoxy(x, y);
		TextColor(ColorCode_Green);
		cout << "*";
		return Point{ x,y };
	}
	bool kiemtraMoi(Point moi)
	{
		if (A[0].x == moi.x && A[0].y == moi.y)
			return true;
		return false;
	}
	void them()
	{
		A[DoDai] = A[DoDai - 1];
		DoDai++;
	}
};

int main()
{
    CONRAN r;
    int Huong = 0;
	int diem = 0;
    char t;
	cout << "\t\t\t\t\t\t\tGAME SNAKE" << endl;
	TextColor(default_ColorCode);
	r.vetuong();
	Point moi = r.Moi();
    while (1){
        if (kbhit()){
            t = getch();
            if (t=='a') Huong = 2;
            if (t=='w') Huong = 3;
            if (t=='d') Huong = 0;
            if (t=='s') Huong = 1;
        }
        Point ToaDoCuoiCu = r.DiChuyen(Huong);
		r.Ve(ToaDoCuoiCu);
		if (r.kiemtraThua()) break;
		if (r.kiemtraMoi(moi))
		{
			TextColor(default_ColorCode);
			moi = r.Moi();
			r.them();
			diem++;
			gotoxy(Trai, Duoi + 3);
			cout << "DIEM: " << diem;

		}
		Sleep(100);
    }
	r.xuliThua();
	cout << "\t\t\t\t\t\tDiem cua ban la: " << diem;
    return 0;
}


